
public class TextWriter {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String[] words = {
				"Hello",
				"World!"
		};
		
		writeLine(words);
		write(words);
	}
	
	public static void writeLine(String[] lines){
		
		for(String line : lines){
			System.out.println(line);
		}
		
		System.out.println(lines.length + " lines printed.");
		
	}
	
	public static void write(String[] words){
		
		for(String word : words){
			System.out.print(word + " ");
		}
		System.out.println();
		System.out.println(words.length + " words printed.");
		
	}
	

}
